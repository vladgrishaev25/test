var swiperPartners = new Swiper('.js-swiper-patners', {
    slidesPerView: "auto",
    loop: true,
    spaceBetween: 16,
    loopFillGroupWithBlank: true,
});
  
var swiperPublications = new Swiper('.js-swiper-publications', {
    slidesPerView: "auto",
    loop: true,
    spaceBetween: 16,
    loopFillGroupWithBlank: true,
});
  
$(function(){
    let burgMenu = document.querySelector('.js-burg-menu')

    $(burgMenu).click(function() {
        $('.header__menu-wrap').slideToggle()
    });
})